use Test::More tests => 3;

use Text::Diff;

use Parasolc qw(autor_curs crea_tex);

{
    my $font = 'fonts/eso/PRAGA 2013.txt';
    my $file_out = 'eso/905_praga_2013.tex';
    
    unlink $file_out or die "$! $file_out"
        if -e $file_out;
    
    $Parasolc::DEBUG=0;
    my $file_tex = crea_tex($font,$font);
    
    ok($file_tex);
    ok($file_tex eq $file_out,"tex: $file_tex, exp: $file_out") and do {
    
        my $diff = diff($file_tex,"expected/$file_tex");
        ok(!$diff,$diff);
    };
     
    
}


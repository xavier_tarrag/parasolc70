use strict;
use warnings;
use Module::Build;

my $builder = Module::Build->new(
    module_name         => 'Parasolc',
    license             => 'perl',
    dist_author         => 'Francesc Guasch <frankie@etsetb.upc.edu>',
    dist_version_from   => 'lib/Parasolc.pm',
    requires => {
        'Test::More' => 0,
        'version'    => 0,
    },
    add_to_cleanup      => [ 'Parasolc-*' ],
);

$builder->create_build_script();

package Parasolc;

use Exporter 'import';
use strict;
use locale;

our @EXPORT_OK = qw(autor_curs possible_curs crea_tex prefix_seccio);


use warnings;
use Carp;

use version; our $VERSION = qv('0.0.3');
our $DEBUG;

######################################################################

our $RE_CURS =qr/(prim.*ria|eso|parvulari|escola solc|coordinaci..? escola)/i;



=head2 autor_curs

Retorna l'autor i el curs donada una línia

=cut

sub autor_curs {
    my $linia = shift;
    return if !possible_curs($linia);
    my $pre_autor = shift;
    my $cont = 0;

    $linia =~ s/^\s+//;
	warn "Possible curs a '$linia'\n"   if $DEBUG;
    my ($autor,$curs);

    warn "intent ".++$cont." $linia\n"  if $DEBUG;
    ($curs) = $linia =~ /^\s*(escola\s+solc.*)\s*/i;
    return ($autor or $pre_autor,$curs) if $curs;

    warn "intent ".++$cont." $linia\n"  if $DEBUG;
	($autor,$curs) = $linia =~ /\s*(.*),\s*(\d.*?)\s*$/;
    if ( $curs ) {
        $autor = '' if ! $autor;
        warn "autor: '$autor', curs: '$curs'\n" if $DEBUG;
        return ($autor,$curs);
    }
    warn "intent ".++$cont." $linia\n"  if $DEBUG;
    ($curs) = $linia =~ / de (\d.*?$RE_CURS) /;
    return ($autor or $pre_autor,$curs) if $curs;

    warn "intent lletres '$linia'"  if $DEBUG;
    my ($curs1,$curs2) = $linia =~ /(primer|segon|tercer|quart|cinqu.)\s+de\s+($RE_CURS)/i;
    if ($curs1 && $curs2) {
        $curs1 = lc($curs1);
        $curs2 = lc($curs2);
        $curs2 = 'primària' if $curs2 =~ /prim..?ria/i;
        $curs = "\u$curs1 de \u$curs2";
        $autor = ($pre_autor or '');
        return ($autor ,$curs);
    }

    warn "intent ".++$cont." $linia\n"  if $DEBUG;
	($curs) = $linia =~ /^\s*(.*?)\.?\s*$/	if !$curs;
    return ($autor or $pre_autor,$curs) if $curs;

}

=head2 prefix_seccio

	Numero prefix de la seccio per al pagesof

=cut

sub prefix_seccio {
	my $seccio = shift or die "Falta seccio";

	return 7 	if $seccio =~ /parvulari/i;
	return 5 	if $seccio =~ /prim.*ria/i;
	return 9	if $seccio =~ /eso/i;
    return 4    if $seccio =~ /angl.*s/i;
    return 2    if $seccio =~ /fem.escola|escola/i;

    confess "No trobo la seccio per '$seccio'";
}


=head2 possible_curs

Retorna si a la linia actual potser surt el curs

=cut

sub possible_curs {
    my $linia = shift;
    return 1 if defined $linia
            && ( $linia =~ m{ de $RE_CURS}i
                ||$linia =~ m{\d(er|n|t|r)\s+d..?.?ESO}i
				||$linia =~ m{\de?r $RE_CURS}
				||$linia =~ m{\d(st|nd) $RE_CURS}
                ||$linia =~ m{\d(st|nd|rd)(\sof)? $RE_CURS}
                ||$linia =~ m{^\s*escola\s*solc\s*[\d\-\_\s]*$}i
                ||$linia =~ m{^\s*coordinaci.*escola\s*$}i
				) ;
        
    return 0;
}

=head2 crea_tex

    Lis passes un arxiu de text i crea un .tex

=cut

sub crea_tex {
	my $file_font = shift or die "Falta file font";
	my $file = shift or die "Falta file";

	my ($titol,$subtitol,$autor,$curs,$curs_alt);
	my @text;

	warn "file: $file\n"	if $DEBUG;
	open my $txt,'<',$file or die "$! $file";
	while (my $linia = <$txt>) {
		next if $linia !~ /\w|\d/;
        warn"$linia\n"  if $DEBUG;
		($curs_alt) = _curs_alternatiu($linia,$curs_alt)    if !$curs_alt;
		if (!$titol || ! defined $subtitol) {
			chomp $linia;
			if (! $titol ) {
                $linia =~ s/^\W*//;
				($titol) = $linia =~ /\s*(.+)\s*/;
				next    if length $titol < 80;
                push @text,("$linia\n");
                ($titol) = $file =~ m{(.*)\.\w+$};
                $subtitol = '';
                next;
			}
			($subtitol) = $linia =~ /\s*(.*?)\./;
            $subtitol = '' if $subtitol && length $subtitol > 80;
			push @text,("$linia\n");
			next;
		}
		chomp $linia;
		my $vocals ='ÀÈÌÒÙÁÉÍÓÚàèìòùáéíóúÄËÏÖÜäëïöü';
		my $re =qr/^\s*([$vocals\w]+)\s([$vocals\w]+)\w*([$vocals\w]*)(,.*)?$/li;
		if ($linia =~ /$re/) {
           warn "Possible autor a '$linia' [curs_alt=".($curs_alt or '<undef>')."]"
                if $DEBUG;
			$autor .= " i " if $autor;
			$autor .= "$1 $2";
			$autor .= " $3" if $3;
            my $cognom2 = ($3 or '');
            if (!$4 && $curs_alt) {
#                warn "autor amb curs $1 $2";
                push @text,("\\authorandplace{$1 $2 $cognom2}{$curs_alt}\n");
                next;
            }
		}
		if (!$curs_alt && length $linia < 40 && possible_curs($linia)){
            warn "Possible curs a '$linia'";
            ($autor,$curs) = autor_curs($linia,$autor);
            if ($autor && $curs && length $linia < 40) {
                warn "autor: $autor, curs: $curs";
                push @text,("\\authorandplace{$autor}{$curs}\n");
			    next;
            }
		}
		warn "$linia\n" if $DEBUG;
		push @text,("$linia\n");
	}
	close $txt;
	$curs = $curs_alt if !$curs;
    $curs = 'Fem Escola' if !$curs;
	confess("No trobo curs a $file")	if !$curs;
    if (length $titol<4) {
        ($titol) = $file_font =~ m{.*/(.*)\.\w+$};
    }
    $titol =~ s/\&/\\\&/g;
	return _imprimeix_tex($file_font,$titol,$subtitol,$autor,$curs,\@text);
}

sub _curs_alternatiu {
    my $linia = shift;
    my $pre_curs = shift;
    my ($autor,$curs) = autor_curs($linia);
    $curs = $pre_curs if !$curs;
    return $curs;
}

=head2 file_tex

    Retorna el nom d'arxiu tex donat un títol

=cut

sub file_tex {
	my $titol = shift or die "Falta titol";
	my $file = '';
	for (split /\s+/,$titol) {
		next if /^\w+$/ && length($_) <3 && !/\d/;
		$file .= '_' if length $file;
#        tr/áéíóú//;
#        tr/àèìòù//;
		s/[^A-Za-z0-9]//g;
        s/^\s*(.*?)\s*$/$1/;
        s/^_*//;
        s/_*$//;
		$file .= lc($_); 
        last if length $file > 20;
	}
	$file =~ s/__+/_/g;

    confess "No aconsegueixo generar un filename desde '$titol'"
        if !length$file;
	return $file;
}

sub _pagesof {
	my $seccio = shift or die $!;
	opendir my $dir , dir_seccio($seccio) or do {
            mkdir dir_seccio($seccio) or die "$! ".dir_seccio($seccio);
            return prefix_seccio($seccio)*100;
    };
	my @arxius = readdir $dir;
	closedir $dir;
	die "No trobo arxius a ".dir_seccio($seccio) if !scalar(@arxius);
	my ($darrer);
    for (@arxius) {
        my ($trobat) =/^(\d+).*tex$/;
        $darrer = $trobat if $trobat && (!$darrer || $trobat > $darrer);
	}
    $darrer = 0 if !$darrer;
#	die "No trobo darrer a @arxius" if !$darrer;
	$darrer =~ s/(\d+).*/$1/;
    $darrer += prefix_seccio($seccio)*100   if $darrer < 100;
	return $darrer + 5;

}


sub _imprimeix_tex {

	my ($file_font,$titol,$subtitol,$autor,$curs,$text) = @_;

    warn "imprimint tex de '$file_font'"    if $DEBUG;
    confess "Necessito el titol" if !$titol;

	my ($seccio) = $file_font =~ m{fonts/(.*?)/};
    $seccio = mira_seccio($curs)        if !$seccio;
    die "No trobo la seccio" if !$seccio;

    $seccio = _maquea_seccio($seccio);
	my $pagesof = _pagesof($seccio);

	my $file_tex = dir_seccio($seccio).
					"/$pagesof".'_'.file_tex($titol).'.tex' ;
	open my $tex,'>',$file_tex
			or die "$! $file_tex";
	print $tex "" 
		."%doc: $file_font\n"
		."\\newpage\n"
		."\n"
		.'\begin{news}'."\n"
		.'{2} %columnes'."\n"
		."{".($titol or '[undef]')."} % titol\n"
		."{".($subtitol or '[undef]')."} % subtitol\n"
		."{$seccio} % seccio\n"
		."{$pagesof} %pagesof\n"
		."% autor: ".($autor or '[undef]')."\n"
		."% curs: ".($curs or '[undef]')."\n"
		."\n"
		.join("\n",@$text)
		."\n";

	print $tex "\\authorandplace{".($autor or '')."}{$curs}\n"
        if !$autor || length $autor < 60;
	print $tex "\n"
		.'\end{news}'."\n"
		;
	close $tex;
    print "traspassat $file_tex\n\n";
	return $file_tex;
}


=head2 mira_seccio

    Retorna la seccio d'un curs

=cut

sub mira_seccio {
	my $curs = shift or croak "Falta el curs";

    return "Anglès"
        if ($curs =~ /\d(st|nd|rd)/);

    return 'Fem Escola' if $curs =~ /escola solc|fem escola/i;

	my ($seccio) = $curs =~ / de (.*)(\W|$)/i;
	return _maquea_seccio($seccio)	if _seccio_valida($seccio);

	($seccio) = $curs =~ /\d\w* .*(prim..?ria|ESO|parvulari)/;
	return _maquea_seccio($seccio)  if _seccio_valida($seccio);

    die "No se a quina seccio pertany '$curs'";
}

sub _maquea_seccio {
    my $seccio = shift;
    $seccio = lc($seccio);
    $seccio =~ tr/À/à/;
    $seccio = uc($seccio) if $seccio =~ /eso/i;
    return "\u$seccio";
}

sub _seccio_valida {
    my $seccio = shift;
    return 0 if !$seccio;
    return 1 if $seccio =~ /^Prim..?ria$/i;
    return 1 if $seccio =~ /^ESO$/i;
    return 1 if $seccio =~ /^Angl..?s$/i;
    return 1 if $seccio =~ /^Fem Escola$/i;
}

=head2 dir_seccio

    Diu a quin directori pertany una seccio

=cut

sub dir_seccio {
	my $seccio = shift or die "Falta seccio";

	return 'angles'		if $seccio =~ /angl.*s/i;
	return 'primaria' 	if $seccio =~ /prim.*ria/i;
	return 'parvulari' 	if $seccio =~ /parvulari/i;
	return 'eso' 		if $seccio =~ /eso/i;
	return 'fem_escola' if $seccio =~ /escola/i;
	confess "No trobo la seccio per '$seccio'";
}

1; # Magic true value required at end of module
__END__

=head1 NAME

Parasolc - [One line description of module's purpose here]


=head1 VERSION

This document describes Parasolc version 0.0.1


=head1 SYNOPSIS

    use Parasolc;

=for author to fill in:
    Brief code example(s) here showing commonest usage(s).
    This section will be as far as many users bother reading
    so make it as educational and exeplary as possible.
  
  
=head1 DESCRIPTION

=for author to fill in:
    Write a full description of the module and its features here.
    Use subsections (=head2, =head3) as appropriate.


=head1 INTERFACE 

=for author to fill in:
    Write a separate section listing the public components of the modules
    interface. These normally consist of either subroutines that may be
    exported, or methods that may be called on objects belonging to the
    classes provided by the module.


=head1 DIAGNOSTICS

=for author to fill in:
    List every single error and warning message that the module can
    generate (even the ones that will "never happen"), with a full
    explanation of each problem, one or more likely causes, and any
    suggested remedies.

=over

=item C<< Error message here, perhaps with %s placeholders >>

[Description of error here]

=item C<< Another error message here >>

[Description of error here]

[Et cetera, et cetera]

=back


=head1 CONFIGURATION AND ENVIRONMENT

=for author to fill in:
    A full explanation of any configuration system(s) used by the
    module, including the names and locations of any configuration
    files, and the meaning of any environment variables or properties
    that can be set. These descriptions must also include details of any
    configuration language used.
  
Parasolc requires no configuration files or environment variables.


=head1 DEPENDENCIES

=for author to fill in:
    A list of all the other modules that this module relies upon,
    including any restrictions on versions, and an indication whether
    the module is part of the standard Perl distribution, part of the
    module's distribution, or must be installed separately. ]

None.


=head1 INCOMPATIBILITIES

=for author to fill in:
    A list of any modules that this module cannot be used in conjunction
    with. This may be due to name conflicts in the interface, or
    competition for system or program resources, or due to internal
    limitations of Perl (for example, many modules that use source code
    filters are mutually incompatible).

None reported.


=head1 BUGS AND LIMITATIONS

=for author to fill in:
    A list of known problems with the module, together with some
    indication Whether they are likely to be fixed in an upcoming
    release. Also a list of restrictions on the features the module
    does provide: data types that cannot be handled, performance issues
    and the circumstances in which they may arise, practical
    limitations on the size of data sets, special cases that are not
    (yet) handled, etc.

No bugs have been reported.

Please report any bugs or feature requests to
C<bug-parasolc@rt.cpan.org>, or through the web interface at
L<http://rt.cpan.org>.


=head1 AUTHOR

Francesc Guasch  C<< <frankie@etsetb.upc.edu> >>


=head1 LICENCE AND COPYRIGHT

Copyright (c) 2013, Francesc Guasch C<< <frankie@etsetb.upc.edu> >>. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself. See L<perlartistic>.


=head1 DISCLAIMER OF WARRANTY

BECAUSE THIS SOFTWARE IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY
FOR THE SOFTWARE, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN
OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES
PROVIDE THE SOFTWARE "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER
EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE
ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE SOFTWARE IS WITH
YOU. SHOULD THE SOFTWARE PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL
NECESSARY SERVICING, REPAIR, OR CORRECTION.

IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING
WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR
REDISTRIBUTE THE SOFTWARE AS PERMITTED BY THE ABOVE LICENCE, BE
LIABLE TO YOU FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL,
OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE
THE SOFTWARE (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING
RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A
FAILURE OF THE SOFTWARE TO OPERATE WITH ANY OTHER SOFTWARE), EVEN IF
SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGES.
